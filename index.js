/* Variables de entorno de Node y de el archivo .env */
require('dotenv').config();
const express = require('express');
/* CORS */
const cors = require('cors');
const { dbConection } = require('./db/config');

/* Crear servidor express */
const app = express();

/* Configurar CORS */
app.use(cors());
/* CAPERTA PUBLICA */
app.use(express.static('public'));
/* Lectura del BODY */
app.use(express.json());

// Base de datos
dbConection();


// RUTAS   
app.use('/api/usuarios', require('./routes/usuarios'));
app.use('/api/login', require('./routes/auth'));
app.use('/api/hospitales', require('./routes/hospitales'));
app.use('/api/medicos', require('./routes/medicos'));
app.use('/api/todo', require('./routes/busquedas'));
app.use('/api/upload', require('./routes/uploads'));


app.listen(process.env.PORT, () => {
    console.log('Servidor corrinedo en el puerto ' + process.env.PORT);
});