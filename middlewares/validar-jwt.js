const { response } = require('express');
const jwt = require('jsonwebtoken');
const Usuario = require('../models/usuario');

const validarJWT = (req, res = response, next) => {
    // leer el token
    const token = req.header('x-token');
    // validamos si se envia token en el header
    if (!token) {
        return res.status(401).json({
            ok: false,
            msg: 'No hay token en la peticion'
        });
    }
    // verificar el token
    try {
        const { uid } = jwt.verify(token, process.env.JWT_SECRET);
        // le pasamos un nuevo elemento a request
        req.uid = uid;
        // siempre se debe poner el next para que funcione el middleware
        next();
    } catch (error) {
        return res.status(401).json({
            ok: false,
            msg: 'Token no valido'
        })
    }

}

const validarADMIN_ROL = async (req, res = response, next) => {
    const uid = req.uid;
    try {

        const usuarioDB = await Usuario.findById(uid);

        if (!usuarioDB) {
            return res.status(404).json({
                ok: false,
                msg: 'Usuario no existe'
            }
            );
        }

        if (usuarioDB.rol !== 'ADMIN_ROLE') {
            return res.status(403).json({
                ok: false,
                msg: 'No tiene privilegios'
            }
            );
        }

        next();

    } catch (error) {
        return res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });
    }
}

const validarADMIN_ROL_o_MismoUsuario = async (req, res = response, next) => {
    const uid = req.uid;
    const id = req.params.id;
    try {

        const usuarioDB = await Usuario.findById(uid);

        if (!usuarioDB) {
            return res.status(404).json({
                ok: false,
                msg: 'Usuario no existe'
            }
            );
        }
        /* actualiza si el rol es igual al admin_role o tiene el mismo id que viene en los params */
        if (usuarioDB.rol === 'ADMIN_ROLE' || uid === id) {
            next();
        } else {
            return res.status(403).json({
                ok: false,
                msg: 'No tiene privilegios'
            }
            );
        }

    } catch (error) {
        return res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });
    }
}

module.exports = {
    validarJWT,
    validarADMIN_ROL,
    validarADMIN_ROL_o_MismoUsuario
}