const { response } = require('express');
const { validationResult } = require('express-validator');

const validarCampo = (req, res = response, next) => {

    const errores = validationResult(req);

    if (!errores.isEmpty()) {
        return res.status(400).json({
            ok: false,
            errors: errores.mapped()
        });
    }
    // SI PASA LLAMAMOS EL NEXT
    next();
}

module.exports = {
    validarCampo
}