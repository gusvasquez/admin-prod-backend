const mongoose = require('mongoose');


/* el async hace que retorne una promesa */
const dbConection = async() => {

    try {
        await mongoose.connect(process.env.DB_CNN);
        console.log('db Online');
    } catch (error) {
        console.log(error);
        throw new Error('Error al iniciar la base de datos');
    } 
    
    
}
/* exportarmos la funcion */
module.exports = {
    dbConection
}