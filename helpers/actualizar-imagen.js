const Usuario = require('../models/usuario');
const Medico = require('../models/medico');
const Hospital = require('../models/hospital');
const fs = require('fs');

const borrarImagen = (path) => {
    /* SI EXISTE LA IMAGEN ANTERIOR LA BORRA */
    if (fs.existsSync(path)) {
        fs.unlinkSync(path);
    }
}

const actualizarImagen = async (tipo, id, nombreArchivo) => {

    let pathViejo = '';

    switch (tipo) {
        case 'medicos':
            const medico = await Medico.findById(id);
            if (!medico) {
                return false;
            }
            pathViejo = `./uploads/medicos/${medico.image}`;
            borrarImagen(pathViejo);
            medico.image = nombreArchivo;
            await medico.save();
            return true;
            break;
        case 'hospitales':
            const hospital = await Hospital.findById(id);
            if (!hospital) {
                return false;
            }
            pathViejo = `./uploads/hospitales/${hospital.image}`;
            borrarImagen(pathViejo);
            hospital.image = nombreArchivo;
            await hospital.save();
            return true;
            break;
        case 'usuarios':
            const usuario = await Usuario.findById(id);
            if (!usuario) {
                return false;
            }
            pathViejo = `./uploads/usuarios/${usuario.image}`;
            borrarImagen(pathViejo);
            usuario.image = nombreArchivo;
            await usuario.save();
            return true;
            break;
    }
}

module.exports = {
    actualizarImagen
}