const getMenuFrontEnd = (role='USER_ROLE') => {
    const menu = [
        {
            titulo: 'Dashboard', icono: 'mdi mdi-gauge', submenu: [
                { titulo: 'Main', url: '/' },
                { titulo: 'ProgresBar', url: '/dashboard/progress' },
                { titulo: 'Promesas', url: '/dashboard/promesas' },
                { titulo: 'Gráficas', url: '/dashboard/grafica1' },
                { titulo: 'Rxjs', url: '/dashboard/rxjs' }
            ]
        },
        {
            titulo: 'Mantenimientos', icono: 'mdi mdi-folder-lock-open', submenu: [
                //{ titulo: 'Usuarios', url: 'usuarios' },
                { titulo: 'Hospitales', url: 'hospitales' },
                { titulo: 'Medicos', url: 'medicos' }
            ]
        }
    ];
    if(role==='ADMIN_ROLE'){
        menu[1].submenu.unshift({ titulo: 'Usuarios', url: 'usuarios' });
    }

    return menu;
}

module.exports ={
    getMenuFrontEnd
}