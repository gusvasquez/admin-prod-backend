const { response } = require('express');
const Hospital = require('../models/hospital');


const getHospital = async (req, res = response) => {

    const hospitales = await Hospital.find()
        /* Con el populate obtenemos el usuario que creeo */
        .populate('usuario', 'nombre img');

    res.json({
        ok: true,
        hospitales: hospitales
    });
}

const crearHospital = async (req, res = response) => {
    const uid = req.uid;
    const hospital = new Hospital({ usuario: uid, ...req.body });

    try {

        const hospitalDB = await hospital.save();

        res.json({
            ok: true,
            hospital: hospitalDB
        });

    } catch (error) {
        return res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });
    }
}

const actualizarHospital = async (req, res = response) => {

    /* Id del Hospital */
    const id = req.params.id;
    /* Id del usuario que lo esta actualizando */
    const uid = req.uid;

    try {
        const hospital = await Hospital.findById(id);
        if (!hospital) {
            return res.status(404).json({
                ok: false,
                msg: 'Hospital no encontrado'
            });
        }

        const cambiosHospital = { ...req.body, usuario: uid };
        // en new: true regresa el ultimo dato actualizado
        const hospitalActualizado = await Hospital.findByIdAndUpdate(id, cambiosHospital, { new: true });

        return res.json({
            ok: true,
            hospital: hospitalActualizado
        });
    } catch (error) {
        return res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });
    }


}

const borrarhospital = async (req, res = response) => {

    /* Id del Hospital */
    const id = req.params.id;

    try {
        const hospital = await Hospital.findById(id);
        if (!hospital) {
            return res.status(404).json({
                ok: false,
                msg: 'Hospital no encontrado'
            });
        }
        await Hospital.findByIdAndDelete(id);

        return res.json({
            ok: true,
            msg: 'Hospital Eliminado'
        });

    } catch (error) {
        return res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });
    }
}


module.exports = {
    getHospital,
    crearHospital,
    actualizarHospital,
    borrarhospital
}