const { response } = require('express');
const Medico = require('../models/medico');

const getMedicos = async (req, res = response) => {
    const medicos = await Medico.find().populate('usuario', 'nombre img').populate('hospital', 'nombre image');
    res.json({
        ok: true,
        medicos: medicos
    });
}

const crearMedico = async (req, res = response) => {
    // id del usuario que lo creo
    const uid = req.uid;
    const medico = new Medico({ usuario: uid, ...req.body });
    try {
        const medicoDB = await medico.save();
        return res.json({
            ok: true,
            medico: medicoDB
        });
    } catch (error) {
        return res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });
    }

}

const actualizarMedico = async (req, res = response) => {

    // id del medico
    const id = req.params.id;
    /* Id del usuario que lo esta actualizando */
    const uid = req.uis;

    try {

        const medico = await Medico.findById(id);

        if (!medico) {
            return res.status(404).json({
                ok: false,
                msg: 'No se encontro ningun Medico con ese ID'
            });
        }
        const cambiosMedicos = { ...req.body, usuario: uid };
        const medicoActualizado = await Medico.findByIdAndUpdate(id, cambiosMedicos, { new: true });

        return res.json({
            ok: true,
            medico: medicoActualizado
        });


    } catch (error) {
        return res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });
    }

}

const borrarMedico = async (req, res = response) => {
    // id del medico
    const id = req.params.id;
    try {
        const medico = await Medico.findById(id);
        if (!medico) {
            return res.status(404).json({
                ok: false,
                msg: 'No se encontro ningun Medico con ese ID'
            });
        }
        await Medico.findByIdAndDelete(id);
        return res.json({
            ok: true,
            msg: 'Medico eliminado'
        });
    } catch (error) {
        return res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });
    }
}

const getMedicosById = async (req, res = response) => {
    // id del medico
    const id = req.params.id;
    try {
        const medico = await Medico.findById(id).populate('usuario', 'nombre img').populate('hospital', 'nombre image');
        if (!medico) {
            return res.status(404).json({
                ok: false,
                msg: 'No se encontro ningun Medico con ese ID'
            });
        }
        return res.json({
            ok: true,
            medico
        });

    } catch (error) {
        return res.status(500).json({
            ok: false,
            msg: 'Hable con el Administrador'
        });
    }


}


module.exports = {
    getMedicos,
    crearMedico,
    actualizarMedico,
    borrarMedico,
    getMedicosById
}