const { response } = require('express');
const Usuario = require('../models/usuario');
const bcrypt = require('bcryptjs');
const { generarJsonToken } = require('../helpers/jwt');
const { googleVerify } = require('../helpers/google-verify');

const { getMenuFrontEnd } = require('../helpers/menu-frontend');

const login = async (req, res = response) => {

    const { email, password } = req.body;

    try {
        // verificar email
        const usuarioDB = await Usuario.findOne({ email: email });
        /* Se valida si no existe el email */
        if (!usuarioDB) {
            return res.status(404).json({
                ok: false,
                msg: 'Email no Valido'
            });
        }

        // verificar contraseña
        // funcion para comparar la contraseña compareSync, la digitada y la que esta en la base de datos
        const valiPassword = bcrypt.compareSync(password, usuarioDB.password);
        if (!valiPassword) {
            return res.status(400).json({
                ok: false,
                msg: 'Contraseña no valida'
            });
        }
        // generar el token -JWT
        /* como regresa una promesa se le pone el await */
        const token = await generarJsonToken(usuarioDB.id);

        res.json({
            ok: true,
            token,
            menu: getMenuFrontEnd(usuarioDB.rol)
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Error inesperado'
        });
    }
}

const googleSignIn = async (req, res = response) => {

    try {
        /* desestructuramos la informacion que nos da google */
        const { email, name, picture } = await googleVerify(req.body.token);

        // validamos si existe el email ya creado en la base de datos
        const usuarioDB = await Usuario.findOne({ email });
        let usuario;

        if (!usuarioDB) {
            usuario = new Usuario({
                nombre: name,
                email: email,
                password: '@@@',
                image: picture,
                google: true
            });
        } else {
            usuario = usuarioDB;
            usuario.google = true;
        }
        // guardar usuario
        await usuario.save();
        // generar token
        const token = await generarJsonToken(usuario.id);

        return res.json({
            ok: true,
            email, name, picture,
            token,
            menu: getMenuFrontEnd(usuario.rol)
        });
    } catch (error) {
        console.log(error);
        return res.status(400).json({
            ok: false,
            msg: 'Token de google no es correcto'
        });
    }


}

const renewToken = async (req, res = response) => {

    const uid = req.uid;
    const token = await generarJsonToken(uid);
    /* Obtener el usuario por id */
    const usuario = await Usuario.findById(uid);

    res.json({
        ok: true,
        token,
        usuario,
        menu: getMenuFrontEnd(usuario.rol)
    });
}

module.exports = {
    login,
    googleSignIn,
    renewToken
}