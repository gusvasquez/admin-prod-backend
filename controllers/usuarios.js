/* Importamos MODELO Usuarios */
const { response } = require('express');
const Usuario = require('../models/usuario');
const bcrypt = require('bcryptjs');
const { generarJsonToken } = require('../helpers/jwt');

const getUsuarios = async (req, res) => {

    /* parametro de paginacion, si no viene nada que coloque el valor 0 */
    const desde = Number(req.query.desde) || 0;
    //  SE PUEDE HACER ASI TAMBIEN
    /* const usuarios = await Usuario.find({}, 'nombre email rol google').skip(desde).limit(5);
    const total = await Usuario.count(); */

    /* Ejecutar todas las promesas */
    const [usuarios, total] = await Promise.all([
        Usuario.find({}, 'nombre email rol google image').skip(desde).limit(5),
        /* total de registros */
        Usuario.countDocuments()
    ]);

    return res.status(200).json({
        ok: true,
        usuarios,
        total
    });
}

const updateUsuario = async (req, res = response) => {

    const uid = req.params.id;
    try {

        const usuarioDB = await Usuario.findById(uid);
        // no existe el usuario
        if (!usuarioDB) {
            return res.status(404).json({
                ok: false,
                msg: 'No existe usuario con ese id'
            });
        }
        // Actualizacion
        const { password, google, email, ...campos } = req.body;
        /* Si es diferente el EMAIL */
        if (usuarioDB.email !== email) {
            /* Si se actualiza el email */
            /* Se valida si existe el email a actualizar */
            const existeEmail = await Usuario.findOne({ email: email });
            if (existeEmail) {
                return res.status(400).json({
                    ok: false,
                    msg: 'Ya existe un usuario con ese email'
                });
            }
        }
        /* Se pone el email a actualizar */
        if (!usuarioDB.google) {
            campos.email = email;
        } else if (usuarioDB.email !== email) {
            return res.status(200).json({
                ok: false,
                msg: 'Usuarios de google no pueden actualizar el correo'
            });
        }
        const usuarioActualizado = await Usuario.findByIdAndUpdate(uid, campos, { new: true });
        return res.json({
            ok: true,
            usuarioActualizado
        });
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            ok: false,
            msg: 'Error inesperado'
        });
    }
}

const crearUsuario = async (req, res = response) => {
    const { email, password, nombre } = req.body;
    try {
        const existeEmail = await Usuario.findOne({ email: email });
        if (existeEmail) {
            return res.status(400).json({
                ok: false,
                msg: 'El correo ya esta registrado'
            });
        }
        const usuario = new Usuario(req.body);
        /* encriptar contraseña */
        // genera un numero aleatorio para encriptar la contraseña
        const salt = bcrypt.genSaltSync();
        // encriptar contraseña
        usuario.password = bcrypt.hashSync(password, salt);

        /* el await espera hasta que esto responda */
        // guardar usuario
        await usuario.save();
        // generar token
        const token = await generarJsonToken(usuario.id);

        return res.status(200).json({
            ok: true,
            usuario: usuario,
            token: token
        });
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            ok: false,
            msg: 'Error inesperados... revisar logs'
        });
    }
}

const borrarUsuario = async (req, res = response) => {
    const uid = req.params.id;
    try {
        // buscar si existe un usuario con el id
        const usuarioDB = await Usuario.findById(uid);
        // no existe el usuario
        if (!usuarioDB) {
            return res.status(404).json({
                ok: false,
                msg: 'No existe usuario con ese id'
            });
        }
        await Usuario.findByIdAndRemove(uid);
        return res.status(200).json({
            ok: true,
            msg: 'Usuario eliminado'
        });
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            ok: false,
            msg: 'Error inesperado, no se pudo eliminar el usuario'
        });

    }

}

module.exports = {
    getUsuarios,
    crearUsuario,
    updateUsuario,
    borrarUsuario
}