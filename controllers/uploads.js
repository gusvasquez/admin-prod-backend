const { response } = require('express');
const { v4: uuidv4 } = require('uuid');
const { actualizarImagen } = require('../helpers/actualizar-imagen');
const path = require('path');
const fs = require('fs');

const fileUpload = (req, res = response) => {


    const tipo = req.params.tipo;
    const id = req.params.id;

    // validar tipo
    const tiposValidos = ['hospitales', 'medicos', 'usuarios'];
    if (!tiposValidos.includes(tipo)) {
        return res.status(400).json({
            ok: false,
            msg: 'No es un medico, usuario u hospital'
        });
    }
    // validar si hay archivos
    if (!req.files || Object.keys(req.files).length === 0) {
        return res.status(400).json({
            ok: false,
            msg: 'No hay ningun archivo'
        });
    }
    // procesar la imagen...
    const file = req.files.imagen;
    const nombreCortado = file.name.split('.');
    const extencionArchivo = nombreCortado[nombreCortado.length - 1];
    // validar extencion
    const extencionesValidas = ['png', 'jpg', 'jpeg', 'gif'];
    if (!extencionesValidas.includes(extencionArchivo)) {
        return res.status(400).json({
            ok: false,
            msg: 'No es una extención permitida'
        });
    }
    // generar el nombre del archivo
    const nombreArchivo = `${uuidv4()}.${extencionArchivo}`;
    // path para guardar la imagen
    const path = `./uploads/${tipo}/${nombreArchivo}`;

    // mover la imagen
    file.mv(path, (err) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                msg: 'Error al mover la imagen'
            });
        }
        // actualizar la base de datos
        actualizarImagen(tipo, id, nombreArchivo);

        return res.json({
            ok: true,
            msg: 'Archivo subido',
            nombreArchivo
        })
    });
}

const retornaImagen = (req, res = response) => {
    const tipo = req.params.tipo;
    const foto = req.params.foto;
    const pathImge = path.join(__dirname, `../uploads/${tipo}/${foto}`);
    // Si existe la imagen
    if (fs.existsSync(pathImge)) {
        res.sendFile(pathImge);
    } else {
        // si no existe la imagen
        const pathImge = path.join(__dirname, `../uploads/no-img.png`);
        res.sendFile(pathImge);
    }
}

module.exports = {
    fileUpload,
    retornaImagen
}