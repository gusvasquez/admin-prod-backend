/*
  RUTA: /api/usuarios 
*/
const { Router } = require('express');
const { getUsuarios, crearUsuario, updateUsuario, borrarUsuario } = require('../controllers/usuarios');
const { check } = require('express-validator');
const { validarCampo } = require('../middlewares/validar-campos');
const { validarJWT, validarADMIN_ROL, validarADMIN_ROL_o_MismoUsuario } = require('../middlewares/validar-jwt');

const router = Router();

router.get('/', validarJWT, getUsuarios);

router.post('/', [
  check('nombre', 'El nombre es obligatorio').not().isEmpty(),
  check('password', 'El password es obligatorio').not().isEmpty(),
  check('email', 'El email es obligatorio').isEmail(),
  validarCampo,
],
  crearUsuario);

router.put('/:id', [
  // primero se  valida el token del usuario
  validarJWT,
  validarADMIN_ROL_o_MismoUsuario,
  check('nombre', 'El nombre es obligatorio').not().isEmpty(),
  check('email', 'El email es obligatorio').isEmail(),
  check('rol', 'El rol es obligatorio').not().isEmpty(),
  validarCampo,
], updateUsuario);

router.delete('/:id',
  // primero se  valida el token del usuario
  validarJWT,validarADMIN_ROL, borrarUsuario);



module.exports = router;