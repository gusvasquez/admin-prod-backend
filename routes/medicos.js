/*
  RUTA: /api/medicos 
*/
const { Router } = require('express');
const { check } = require('express-validator');
const { validarCampo } = require('../middlewares/validar-campos');
const { validarJWT } = require('../middlewares/validar-jwt');
const { getMedicos, crearMedico, actualizarMedico, borrarMedico, getMedicosById } = require('../controllers/medicos');

const router = Router();

router.get('/', [validarJWT], getMedicos);

router.post('/', [validarJWT,
  check('nombre', 'El nombre del Medico es requerido').not().isEmpty(),
  check('hospital', 'El id del hospital id debe de ser valido').isMongoId(), validarCampo], crearMedico);

router.put('/:id', [validarJWT,
  check('nombre', 'El nombre del Medico es requerido').not().isEmpty(),
  check('hospital', 'El id del hospital id debe de ser valido').isMongoId(), validarCampo], actualizarMedico);

router.delete('/:id', validarJWT, borrarMedico);

router.get('/:id', validarJWT, getMedicosById);



module.exports = router;