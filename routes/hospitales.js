/*
  RUTA: /api/hospitales 
*/
const { Router } = require('express');
const { check } = require('express-validator');
const { validarCampo } = require('../middlewares/validar-campos');
const { validarJWT } = require('../middlewares/validar-jwt');
const { getHospital, crearHospital, actualizarHospital, borrarhospital } = require('../controllers/hospitales');

const router = Router();

router.get('/', [validarJWT], getHospital);

router.post('/', [validarJWT, check('nombre', 'El nombre del Hospital es necesario').not().isEmpty(), validarCampo], crearHospital);

router.put('/:id', [validarJWT, check('nombre', 'El nombre del Hospital es necesario').not().isEmpty(), validarCampo], actualizarHospital);

router.delete('/:id', validarJWT, borrarhospital);



module.exports = router;